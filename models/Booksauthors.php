<?php

namespace app\models;

use yii\db\ActiveRecord;

class Booksauthors extends ActiveRecord {
    
    public static function tableName()
    {
        return 'booksauthors';
    }

    public static function getAll() {
        $data = self::find()->all();
        return $data;
    }
    /**
     * Возвращает id всех книг, написанных автором
     */
    public static function getBooksByAuthor($author_id) {
        $books_id = self::find()->where(['author_id' => $author_id])->all();
        return $books_id;
    }

    public static function getAuthorsByBook($book_id) {
        $authors_id = self::find()->where(['book_id' => $book_id])->all();
        return $authors_id;
    }

    public function rules()
    {
        return [
            // тут определяются правила валидации
        ];
    }

}


