<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\base\Model;

class Books extends ActiveRecord {
    
    public static function tableName()
    {
        return 'books';
    }
<<<<<<< HEAD
    public static function getAll() {
        $data = self::find()->all();
        return $data;
    }
    public static function getOne($id) {
        $current = self::find()->where(['id' => $id])->one();
        return $current;
    }

=======

    /**
     * Связь с таблицей authors через промежуточную таблицу booksauthors
     */
    public function getAuthors() {
        return $this->hasMany(Authors::class, ['author_id' => 'author_id'])
                    ->viaTable('booksauthors', ['book_id' => 'book_id']);
    }

    /**
     * Связь с таблицей category через промежуточную таблицу bookscategory
     */
    public function getCategory() {
        return $this->hasMany(Categories::class, ['category_id' => 'category_id'])
                    ->viaTable('bookscategory', ['book_id' => 'book_id']);
    }

    public static function getAll() {
        $data = self::find()
        ->joinwith('authors')
        ->joinWith('category')
        ->all();
        return $data;
    }

    public static function test() {
        //SELECT `books`.`book_id`, `books`.`title`, `booksauthors`.`author_id` AS `author_id` FROM `books` LEFT JOIN `booksauthors` ON `books`.`book_id` = `booksauthors`.`book_id`
        $data = self::find()
        ->joinwith('authors')
        ->joinWith('category')
        ->all();
        return $data;
    }

    public static function getOne($id) {
        $current = self::find()->where(['book_id' => $id])->one();
        return $current;
    }

    public static function getLastBook() {
        $last_book_id = self::find()->max('book_id');
        return $last_book_id;
    }

>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
    public function rules()
    {
        return [
            // тут определяются правила валидации
        ];
    }
<<<<<<< HEAD

=======
 
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
    /**
     * Метод, для нахождения книги по isbn. Возвращает объект книгу
     */
    public static function findOne($isbn) {
        $data = self::find()->where(['isbn' => $isbn])->one();
        return $data;
    }

<<<<<<< HEAD
=======
    public static function getBooksByCategory($category_id) {
        $books = self::find()
        ->select('books.*')
        ->leftJoin('bookscategory', '`bookscategory`.`category_id` = `category`.`category_id`')
        ->where(['`bookscategory`.`category_id`' => $category_id])
        ->all();
        return $books;
    }

>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
}


