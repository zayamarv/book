<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;

class Categories extends ActiveRecord {
    
    public static function tableName()
    {
        return 'category';
    }

    public static function getAll() {
        $data = self::find()->all();
        return $data;
    }
    public static function getOneById($id) {
        $current = self::find()->where(['category_id' => $id])->one();
        return $current;
    }

    public static function getOneByName($author) {
        $current = self::find()->where(['category' => $author])->one();
        return $current;
    }
    /**
     * Возвращает всех авторов книги, id которой передается в параметрах функции
     * Данный метод пока не нужен, так как настроена связь книг с категориями через таблицу bookscategory
     */
    // public static function getCategoriesByBook($book_id) {
    //     $categories = self::find()
    //     ->select('category.*')
    //     ->leftJoin('bookscategory', '`bookscategory`.`category_id` = `category`.`category_id`')
    //     ->where(['`bookscategory`.`book_id`' => $book_id])
    //     ->all();
    //     return $categories;
    // }

    public function rules()
    {
        return [
            // тут определяются правила валидации
        ];
    }

    public static function getMenuItems()
    {
        $category=self::getAll();
    
        $items = [];
        foreach ($category as $var) {
            if ($var->parentCategory == null) 
            {
                array_push($items, [
                    'label' => $var->category,
                    'url' => [Url::to(['/category', 'category_id' => $var->category_id])],
                    'items' => self::getMenuChildrenItems($category, $var->category_id)
                ]);
                
            }
        }
        return $items;
    }

    private static function getMenuChildrenItems($category, $id)
    {
        $items = [];
        foreach ($category as $var) {
            if ($var->parentCategory == $id) 
            {
                array_push($items, [
                    'label' => $var->category,
                    'url' => [Url::to(['/category', 'category_id' => $var->category_id])],
                    'items' => self::getMenuChildrenItems($category, $var->category_id)
                ]);
                
            }
        }
        return $items;
    }

}


