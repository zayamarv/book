<?php

namespace app\models;

use yii\db\ActiveRecord;

class Authors extends ActiveRecord {
    
    public static function tableName()
    {
        return 'authors';
    }


    public static function getAll() {
        $data = self::find()->all();
        return $data;
    }
    public static function getOneById($id) {
        $current = self::find()->where(['author_id' => $id])->one();
        return $current;
    }

    public static function getOneByName($author) {
        $current = self::find()->where(['author' => $author])->one();
        return $current;
    }
    /**
     * Возвращает всех авторов книги, id которой передается в параметрах функции
     * Данный метод пока не нужен, так как настроена связь между книгами и авторами через таблицу booksauthors в модели Books
     */
    // public static function getAuthorsByBook($book_id) {
    //     $authors = self::find()
    //     ->select('authors.*')
    //     ->leftJoin('booksauthors', '`booksauthors`.`author_id` = `authors`.`author_id`')
    //     ->where(['`booksauthors`.`book_id`' => $book_id])
    //     ->all();
    //     return $authors;
    // }

    

    public function rules()
    {
        return [
            // тут определяются правила валидации
        ];
    }

}


