<?php

namespace app\models;

use yii\db\ActiveRecord;

class Bookscategory extends ActiveRecord {
    
    public static function tableName()
    {
        return 'bookscategory';
    }

    public static function getAll() {
        $data = self::find()->all();
        return $data;
    }
    /**
     * Возвращает id всех книг, написанных автором
     */
    public static function getBooksByCategory($category_id) {
        $books_id = self::find()->where(['category_id' => $category_id])->all();
        return $books_id;
    }

    public static function getCategoriesByBook($book_id) {
        $categories_id = self::find()->where(['book_id' => $book_id])->all();
        return $categories_id;
    }


    public function rules()
    {
        return [
            // тут определяются правила валидации
        ];
    }

}


