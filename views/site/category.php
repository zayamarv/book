<?php
use yii\helpers\Url;
use app\models\Authors;
use app\models\Categories;

$this->title = 'Категории';
?>
<div class = "container">
<div class="list-group">
<?php 
foreach($categories as $category):?>
    <?php 
        if($category['items'] != NULL){
            echo "<div class='dropdown show list-group-item'>";
            echo " <a class='btn btn-primary dropdown-toggle' href='".$category['url']."' role='button' id='dropdownMenuLink' data-toggle='dropdown' 
            aria-haspopup='true' aria-expanded='false'>".$category['label']."</a>";
            echo "<div class='dropdown-menu' aria-labelledby='dropdownMenuLink'>";
            foreach($category['items'] as $item){
                echo "<a href='".$item['url']."' class='dropdown-item'>".$item['label']."</a>";
            }
            echo "</div>
            </div>";
        }
        else {
            echo "<a href='".$category['url']."' class='list-group-item'>".$category['label']."</a>";
        }
    ?>
    
<?php endforeach; ?>
</div>
</div>
