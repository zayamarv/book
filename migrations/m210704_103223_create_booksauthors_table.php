<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%booksauthors}}`.
 */
class m210704_103223_create_booksauthors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%booksauthors}}', [
            'book_id' => $this->integer(),
            'author_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%booksauthors}}');
    }
}
