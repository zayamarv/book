<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bookscategory}}`.
 */
class m210705_042335_create_bookscategory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bookscategory}}', [
            'book_id' => $this->integer(),
            'category_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bookscategory}}');
    }
}
