<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 */
class m210628_082013_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
<<<<<<< HEAD
            'id' => $this->primaryKey(),
=======
            'book_id' => $this->primaryKey(),
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
            'title' => $this->string()->notNull(),
            'isbn' => $this->integer(),
            'pageCount' => $this->integer(),
            'publishedDate' => $this->dateTime(),
            'thumbnailUrl' => $this->string(),
            'shortDescription' => $this->text(),
            'longDescription' => $this->text(),
            'status' => $this->string(),
<<<<<<< HEAD
            'authors' => $this->string(),
            'categories' => $this->string(),  
=======
            // 'authors' => $this->string(),
            // 'categories' => $this->string(),  
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
