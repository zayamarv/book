<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Books;
<<<<<<< HEAD
=======
use app\models\Authors;
use app\models\Booksauthors;
use app\models\Categories;
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
use app\models\User;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

/**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = User::findByUsername('admin');
<<<<<<< HEAD
        $books = Books::find()->all();
        return $this->render('index', ['books' => $books, 'user' => $user]);
    }
=======
        $books = Books::getAll();
        
        return $this->render('index', ['books' => $books, 'user' => $user]);
    }
    public function actionCategory() {
        $categories = Categories::getMenuItems();
            return $this->render('category', ['categories' => $categories]);
    }
 
    public function actionBooksCategorySelected($id) {
        $category = Categories::getOneById($id);
        $books = Books::getBooksByCategory($id);
        var_dump($books);
        // return $this->render('booksCategorySelected', ['category' => $category, 'books' => $books]);
    }

>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05

   
    

}
