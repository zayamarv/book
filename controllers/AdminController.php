<?php

namespace app\controllers;
use Yii;
use yii\web\Controller;
use app\models\Books;
use app\models\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;

class AdminController extends Controller
{
<<<<<<< HEAD

=======
 
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
    public function actionAdmin() {
        $user = User::findByUsername('admin');
        Yii::$app->user->login($user);
        if(Yii::$app->user->isGuest) {
            echo "Доступ запрещен";
        }
        else {
            $books = Books::getAll();
            return $this->render('admin', ['books' => $books, 'user' => $user]);
        }
       
    }

    public function actionCreate() {
        $newBook = new Books();
        if($newBook->load(Yii::$app->request->post())) {
            
            $newBook->isbn = $_POST['Books']['isbn'];
            $newBook->title = $_POST['Books']['title'];
            $newBook->pageCount = $_POST['Books']['pageCount'];
            $newBook->publishedDate = $_POST['Books']['publishedDate'];
            $newBook->shortDescription = $_POST['Books']['shortDescription'];
            $newBook->longDescription = $_POST['Books']['longDescription'];
            $newBook->status = $_POST['Books']['status'];
            $newBook->authors = $_POST['Books']['authors'];
            $newBook->categories = $_POST['Books']['categories'];
            $newBook->save();
            return $this->redirect(['admin']);
        }
        //Если страница отображается первый раз
        else {
            return $this->render('create', ['newBook' => $newBook]);
        }

    }

    public function actionEdit($id) {
        $current = Books::getOne($id);

        if($current->load(Yii::$app->request->post())) {
            
            $current->isbn = $_POST['Books']['isbn'];
            $current->title = $_POST['Books']['title'];
            $current->pageCount = $_POST['Books']['pageCount'];
            $current->publishedDate = $_POST['Books']['publishedDate'];
            $current->shortDescription = $_POST['Books']['shortDescription'];
            $current->longDescription = $_POST['Books']['longDescription'];
            $current->status = $_POST['Books']['status'];
            $current->authors = $_POST['Books']['authors'];
            $current->categories = $_POST['Books']['categories'];
            $current->save();
            return $this->redirect(['admin']);
        }
        //Если страница отображается первый раз
        else {
            return $this->render('edit', ['current' => $current]);
        }
    }

    public function actionDelete($id)
    {
        $current = Books::getOne($id);
        $current->delete();
        return $this->redirect(['admin']);
    }



    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

/**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
