<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

// use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Books;
<<<<<<< HEAD
=======
use app\models\Authors;
use app\models\Booksauthors;
use app\models\Bookscategory;
use app\models\Categories;

>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
// use app\models\UploadForm;
// use yii\web\UploadedFile;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ParseController extends Controller
{
    /**
     * Для работы этой функции необходимо поправить файл php.ini. Параметр error_reporting не должен включать в себя E_WARNING. Подробнее по ссылке - https://www.php.net/manual/ru/function.file-get-contents.php
     * @return int Exit code
     */
    public function actionIndex($src = 'https://gitlab.com/prog-positron/test-app-vacancy/-/raw/master/books.json')
    {
        $file_json = file_get_contents($src);
        $file_json = str_replace('$date', 'date', $file_json);
        $file_json = json_decode($file_json);

<<<<<<< HEAD
        // $file_json = [$file_json[2], $file_json[4]];
=======
        // $file_json = [$file_json[0], $file_json[1], $file_json[2]];
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
      
        foreach($file_json as $item) {
            if($item->isbn == Books::findOne($item->isbn)->isbn) {//Если нашлась в БД книга с тем же isbn, то ничего не делаем
            }
            else {
                $book = new Books();
                $book->title = $item->title;
                $book->isbn = $item->isbn;
                $book->pageCount = $item->pageCount;
                $book->publishedDate = date('Y-m-d h:m:s', strtotime($item->publishedDate->date));
                
                if($item->thumbnailUrl != NULL) {
                    $imagename = explode('/',$item->thumbnailUrl);
                    $this->upload($item->thumbnailUrl, $imagename[count($imagename)-1]);
                    $book->thumbnailUrl = $imagename[count($imagename)-1];
                }

                $book->shortDescription = $item->shortDescription;
                $book->longDescription = $item->longDescription;
                $book->status = $item->status;
<<<<<<< HEAD
                foreach($item->authors as $author) {
                    $book->authors .= $author.","; 
                }
                $book->authors = trim($book->authors, ',');
                if($item->categories != NULL){
                    foreach($item->categories as $category) {
                        $book->categories .= $category.","; 
                    }
                    $book->categories = trim($book->categories, ',');
                }
                else {
                    $book->categories = 'Новинки';
                }
                
                $book->save();
            }
            
        }
        echo "Parsing is complete.";
=======
                
                $book->save();

                
                
                //Парсим авторов
                foreach($item->authors as $author) {
                    if($author != "") {
                        $booksauthors = new Booksauthors();
                        //Если текущего автора нет в таблице авторов, то добавить его
                        if(!Authors::getOneByName($author)) {
                            $authors = new Authors();
                            $authors->author = $author;
                            $authors->save();
                        }
                        //В таблицу booksauthors внести сопоставление последнюю добавленную книгу написали следующие авторы
                        $booksauthors->book_id = Books::getLastBook();
                        $booksauthors->author_id = Authors::getOneByName($author)->author_id;
                        $booksauthors->save();
                    }
                }
                
                //Парсим категории
                for($i = 0; $i<count($categories = $item->categories); $i++) {
                    $bookscategory = new Bookscategory();
                    //Если текущей категории нет в таблице категорий, то добавить ее
                    if(!Categories::getOneByName($categories[$i])) {
                        $category = new Categories();
                        $category->category = $categories[$i];
                        //Если категорий больше 1, то считать каждую следующую категорию подкатегорией предыдущей
                        if($i > 0) {
                            $category->parentCategory = Categories::getOneByName($categories[$i-1])->category_id;
                        }                    
                        $category->save();
                    }
                    //В таблицу bookscategory внести сопоставление - последняя добавленная книга относится к следующим категориям
                    $bookscategory->book_id = Books::getLastBook();
                    $bookscategory->category_id = Categories::getOneByName($categories[$i])->category_id;
                    $bookscategory->save();
                    
                }

            }
            
        }
        echo "Parsing is complete.\n";
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
        return ExitCode::OK;
    }

    private function upload($src, $imagename)
    {
<<<<<<< HEAD
        $filename = 'web/uploads/'.$imagename;
        //создаем пустой файл для помещения в него будущего файла jpg.
        fopen($filename, 'a');
        fclose($filename);
        $file_image = file_get_contents($src); //Считываем внешний файл в строку.
        //Проверяем доступен ли файл для записи
        if (is_writable($filename)) {
            if (!$handle = fopen($filename, 'a')) {
                echo "Не могу открыть файл ($filename)";
                exit;
           }
           //Записываем считанную строку в файл-пустышку
           if (fwrite($handle, $file_image) === FALSE) {
                echo "Не могу произвести запись в файл ($filename)";
                exit;
            }
            
            echo "Файл ($src) успешно загружен в файл ($filename)\n";

            fclose($handle);

        }   else {
                echo "Файл $filename недоступен для записи";
            }

=======
        $filename = '../public_html/uploads/'.$imagename;
        //создаем пустой файл для помещения в него будущего файла jpg.
        $file = fopen($filename, "a");
        //Тут стоит добавить проверку, существет ли файл с таким именеми идентичен ли он тому, который пытаемся скачать.
        fclose($file);    
        if(!(md5_file($filename) == md5_file($src))) {

            $file_image = file_get_contents($src); //Считываем внешний файл в строку.

            //Проверяем доступен ли файл для записи
            if (is_writable($filename)) {
                if (!$handle = fopen($filename, 'a')) {
                    echo "Не могу открыть файл ($filename)";
                    exit;
            }
            //Записываем считанную строку в файл-пустышку
            if (fwrite($handle, $file_image) === FALSE) {
                    echo "Не могу произвести запись в файл ($filename)";
                    exit;
                }
                
                echo "Файл ($src) успешно загружен в файл ($filename)\n";

                fclose($handle);

            }   else {
                    echo "Файл $filename недоступен для записи";
                }

        }
    }

    public function actionTest() {
        echo md5_file('../public_html/uploads/ableson.jpg')."\n";
        echo md5_file('https://s3.amazonaws.com/AKIAJC5RLADLUMVRPFDQ.book-thumb-images/ableson.jpg')."\n";
        
        // $file1 = fopen('../public_html/uploads/ableson.jpg', "r");
        // $file_size1 = 0;
        // while(($str = fread($file1, 1024)) != null) {
	
        //     $file_size1 += strlen($str);
            
        //     }
        // echo $file_size1."\n";
        // $file = fopen('https://s3.amazonaws.com/AKIAJC5RLADLUMVRPFDQ.book-thumb-images/ableson.jpg', "r");
        // $file_size = 0;
	
	    // while(($str = fread($file, 1024)) != null) {
	
		// $file_size += strlen($str);
		
	    // }
        // echo $file_size."\n";
        
>>>>>>> 8eec82ef2462fa90f061f3b45dd9ae1c1b680c05
    }

}
